import 'package:lab1_01/lab1_01.dart' as lab1_01;
import 'dart:io';

void main(List<String> arguments) {
  print('MENU');
  print('1. ADD\n2. SUBTRACT\n3. MULTIPLY\n4. DIVIDE\n5. EXIT');
  print('Choice you want to enter :');
  String choice = stdin.readLineSync()!;
  
  if(choice == '1') {
    print('Enter the value for x :');
    int x = int.parse(stdin.readLineSync()!);
    print('Enter the value for y :');
    int y = int.parse(stdin.readLineSync()!);
    print('Sum of the two numbers is :');
    print(x+y);
  } else if(choice == '2') {
    print('Enter the value for x :');
    int x = int.parse(stdin.readLineSync()!);
    print('Enter the value for y :');
    int y = int.parse(stdin.readLineSync()!);
    print('Subtraction results of the two number is :');
    print(x-y);
  } else if(choice == '3') {
    print('Enter the value for x :');
    int x = int.parse(stdin.readLineSync()!);
    print('Enter the value for y :');
    int y = int.parse(stdin.readLineSync()!);
    print('Multiply results of the two number is :');
    print(x*y);
  } else if(choice == '4') {
    print('Enter the value for x :');
    int x = int.parse(stdin.readLineSync()!);
    print('Enter the value for y :');
    int y = int.parse(stdin.readLineSync()!);
    print('Divide results of the two number is :');
    print(x/y);
  } else if(choice == '5') {
    print('Exit');
  }
}
